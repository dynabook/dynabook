'From Cuis 5.0 [latest update: #4253] on 4 August 2020 at 11:27:35 am'!
'Description '!
!provides: 'AltGC' 1 0!
SystemOrganization addCategory: #AltGC!


!classDefinition: #AltGarbageCollector category: #AltGC!
Object subclass: #AltGarbageCollector
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'AltGC'!
!classDefinition: 'AltGarbageCollector class' category: #AltGC!
AltGarbageCollector class
	instanceVariableNames: 'objects'!

!classDefinition: #AltMarker category: #AltGC!
Object subclass: #AltMarker
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'AltGC'!
!classDefinition: 'AltMarker class' category: #AltGC!
AltMarker class
	instanceVariableNames: ''!


!AltGarbageCollector class methodsFor: 'as yet unclassified' stamp: 'GDK 7/21/2020 19:16:37'!
shutDown
super shutDown.
Smalltalk allObjects do: [:o|o triggerEvent: #isVolatile with: [o becomeForward: nil]].! !

!AltGarbageCollector class methodsFor: 'as yet unclassified' stamp: 'jmv 11/16/2016 15:37:15'!
ifTrue: [
					aStream skip: -1.
					atLeastOneDigitRead ifFalse: [self error: 'At least one digit expected here'].
					^neg
						ifTrue: [value negated]
						ifFalse: [value]].
			value _ value * base + digit.
			atLeastOneDigitRead _ true ].
	neg ifTrue: [^ value negated].
	^ value! !

!AltGarbageCollector class methodsFor: 'as yet unclassified' stamp: 'GDK 7/20/2020 12:16:35'!
isDrum: obj
| v |
v _ true.
obj triggerEvent: #isDrum with: [v _ false].
^v! !

!AltGarbageCollector class methodsFor: 'as yet unclassified' stamp: 'GDK 7/20/2020 17:44:34'!
isNonGCAble: obj
| v |
v _ false.
obj triggerEvent: #isNonGCAble with: [v _ true].
Smalltalk allObjects do: [:o|(self object: o includesRecursive: obj) ifTrue: [o triggerEvent: #GCBoundaryCheck withArguments: {obj.[:v2|v _ v2]}]].
^v! !

!AltGarbageCollector class methodsFor: 'as yet unclassified' stamp: 'GDK 7/21/2020 08:32:47'!
mark
^(Smalltalk allObjects collect: [:o|o -> ((((Smalltalk allObjects reject: [:o2|o2 == objects or: [self object: o includesRecursive: o2]]) anySatisfy: [:o2|((1 to: o2 basicSize) anySatisfy: [:f|(o2 basicAt: f) == o]) or: [(1 to: o2 class instVarNames size)  anySatisfy: [:f|(o2 instVarAt: f) == 0]] or: [o2 class == o]]) or: [self isNonGCAble: o]) and: [(self isDrum: o) not])]) asDictionary! !

!AltGarbageCollector class methodsFor: 'as yet unclassified' stamp: 'GDK 7/20/2020 17:29:03'!
markAllAndSweep
((AltMarker allSubclasses, {self} ) collect: [:x|x mark]) do: [:x|self sweep: x].! !

!AltGarbageCollector class methodsFor: 'as yet unclassified' stamp: 'GDK 8/4/2020 10:02:47'!
object: o becomeForward: o2 in: o3
self object: o3 includesRecursive: o report: [:oo :x|self patch: oo x: x target: o2]! !

!AltGarbageCollector class methodsFor: 'as yet unclassified' stamp: 'GDK 8/4/2020 08:56:35'!
object: o includesRecursive: o2
self object: o includesRecursive: o2 report: [:x :y| ^true].
^false! !

!AltGarbageCollector class methodsFor: 'as yet unclassified' stamp: 'GDK 8/4/2020 08:49:08'!
object: o includesRecursive: o2 report: reportBlock
o class == o2 ifTrue: [reportBlock value: o value: {#class}].
(self object: o class includesRecursive: o2 report: reportBlock).
o class instVarNames doWithIndex: [:pr :i|
	(o instVarAt: i) == o2 ifTrue: [reportBlock value: o value: {#instVar. i}].
(self object: (o instVarAt: i) includesRecursive: o2 report: reportBlock).
	].
1 to: o basicSize do: [:bi|
	(o basicAt: bi)== o2 ifTrue: [reportBlock value: o value: {#arrarVar. bi}].
(self object: (o basicAt: bi) includesRecursive: o2 report: reportBlock).
	].
^nil! !

!AltGarbageCollector class methodsFor: 'as yet unclassified' stamp: 'GDK 8/4/2020 09:15:22'!
patch: object x: x target: t
x = {#class} ifTrue: [object primitiveChangeClassTo: t].
x first == #instVar ifTrue: [object instVarAt: x second put: t].
x first == #arrayVar ifTrue: [object basicAt: x second put: t].! !

!AltGarbageCollector class methodsFor: 'as yet unclassified' stamp: 'GDK 7/20/2020 12:23:14'!
startTickler
^[[objects _ Smalltalk allObjects] repeat] fork! !

!AltGarbageCollector class methodsFor: 'as yet unclassified' stamp: 'GDK 7/21/2020 10:13:34'!
sweep: dict
thisContext isSecure ifFalse: [^self].
dict associationsDo: [:a|a value ifFalse: [a key finalize;becomeForward: nil]].! !

!AltMarker class methodsFor: 'as yet unclassified' stamp: 'GDK 7/20/2020 17:28:07'!
mark
^self subclassResponsibility! !
