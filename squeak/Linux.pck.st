'From Cuis 5.0 [latest update: #4253] on 3 August 2020 at 8:58:16 pm'!
'Description '!
!provides: 'Linux' 1 0!
SystemOrganization addCategory: #'Linux-Kernel'!


!classDefinition: #LinuxProcessSpawner category: #'Linux-Kernel'!
Object subclass: #LinuxProcessSpawner
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Linux-Kernel'!
!classDefinition: 'LinuxProcessSpawner class' category: #'Linux-Kernel'!
LinuxProcessSpawner class
	instanceVariableNames: ''!


!LinuxProcessSpawner class methodsFor: 'as yet unclassified' stamp: 'GDK 7/30/2020 19:04:21'!
syscallMap: importDict
^{
'open' -> {#args -> [1]. #deny -> [false]. #value -> [:a|((importDict at: #unixRootDrive) at: a first) object]} asDictionary.
'read' -> {#args -> [3],#deny -> [false]. #value -> [:a|1 to: a third do: [:x|a second at: x put: a first next]]} asDictionary.
'write' -> {#args -> [3]. #deny -> [false]. #value -> [:a|1 to: a third do: [:x|a first nextPut: (a second at: x)]]} asDictionary.
'pipe' -> {#args -> [1]. #deny -> [false]. #value -> [:a|
	|p |
	p _ ReadWriteStream on: ''.
	a first at: 1 put: p;at: 2 put: p.
	]} asDictionary
} asDictionary! !
