'From Cuis 5.0 [latest update: #4253] on 3 August 2020 at 8:56:49 pm'!
'Description '!
!provides: 'Pipes' 1 0!
SystemOrganization addCategory: #Pipes!


!classDefinition: #Pipe category: #Pipes!
Object subclass: #Pipe
	instanceVariableNames: 'source target'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Pipes'!
!classDefinition: 'Pipe class' category: #Pipes!
Pipe class
	instanceVariableNames: ''!


!Pipe methodsFor: 'accessing' stamp: 'GDK 7/23/2020 17:10:32'!
source
	"Answer the value of source"

	^ source! !

!Pipe methodsFor: 'accessing' stamp: 'GDK 7/23/2020 17:10:32'!
source: anObject
	"Set the value of source"

	source _ anObject! !

!Pipe methodsFor: 'accessing' stamp: 'GDK 7/23/2020 17:10:32'!
target
	"Answer the value of target"

	^ target! !

!Pipe methodsFor: 'accessing' stamp: 'GDK 7/23/2020 17:10:32'!
target: anObject
	"Set the value of target"

	target _ anObject! !

!Pipe class methodsFor: 'as yet unclassified' stamp: 'GDK 7/23/2020 18:14:16'!
sFor: val
^(self allSubInstances select: [:i|i source == val]) collect: [:x|x target]! !
