'From Cuis 5.0 [latest update: #4253] on 3 August 2020 at 8:58:11 pm'!
'Description '!
!provides: 'Domains' 1 0!
SystemOrganization addCategory: #Domains!


!classDefinition: #DomainLink category: #Domains!
TextAction subclass: #DomainLink
	instanceVariableNames: 'domain'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Domains'!
!classDefinition: 'DomainLink class' category: #Domains!
DomainLink class
	instanceVariableNames: ''!

!classDefinition: #DomainRequirement category: #Domains!
FeatureRequirement subclass: #DomainRequirement
	instanceVariableNames: 'domain'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Domains'!
!classDefinition: 'DomainRequirement class' category: #Domains!
DomainRequirement class
	instanceVariableNames: ''!

!classDefinition: #Domain category: #Domains!
Object subclass: #Domain
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Domains'!
!classDefinition: 'Domain class' category: #Domains!
Domain class
	instanceVariableNames: ''!

!classDefinition: #AllDomain category: #Domains!
Domain subclass: #AllDomain
	instanceVariableNames: 'domains'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Domains'!
!classDefinition: 'AllDomain class' category: #Domains!
AllDomain class
	instanceVariableNames: ''!

!classDefinition: #LinkDomain category: #Domains!
Domain subclass: #LinkDomain
	instanceVariableNames: 'exists'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Domains'!
!classDefinition: 'LinkDomain class' category: #Domains!
LinkDomain class
	instanceVariableNames: ''!

!classDefinition: #WorldDomain category: #Domains!
Domain subclass: #WorldDomain
	instanceVariableNames: 'world'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Domains'!
!classDefinition: 'WorldDomain class' category: #Domains!
WorldDomain class
	instanceVariableNames: ''!


!DomainLink methodsFor: 'as yet unclassified' stamp: 'GDK 7/6/2020 18:08:22'!
actOnClickFor: anObject
domain exists: domain exists not.
^true! !

!DomainRequirement methodsFor: 'as yet unclassified' stamp: 'GDK 7/7/2020 12:04:44'!
checkRequirement
^super checkRequirement and: [domain exists]! !

!DomainRequirement methodsFor: 'accessing' stamp: 'GDK 7/6/2020 18:05:06'!
domain
	"Answer the value of domain"

	^ domain! !

!DomainRequirement methodsFor: 'accessing' stamp: 'GDK 7/6/2020 18:05:07'!
domain: anObject
	"Set the value of domain"

	domain _ anObject! !

!Domain methodsFor: 'as yet unclassified' stamp: 'GDK 7/6/2020 14:49:18'!
exists
^false! !

!Domain methodsFor: 'as yet unclassified' stamp: 'GDK 7/6/2020 15:32:39'!
forceProtectedMode
^false! !

!Domain methodsFor: 'as yet unclassified' stamp: 'GDK 7/14/2020 20:30:08'!
forcesInputMethod: im
^false! !

!Domain class methodsFor: 'as yet unclassified' stamp: 'GDK 7/6/2020 14:50:12'!
allExisting
^Domain allSubInstances select: [:i|i exists]! !

!Domain class methodsFor: 'as yet unclassified' stamp: 'GDK 7/6/2020 15:00:22'!
forceProtectedMode
^self allExisting inject: false into: [:o :v|o or: [v forceProtectedMode]]! !

!Domain class methodsFor: 'as yet unclassified' stamp: 'GDK 7/15/2020 07:22:47'!
forcesInputMethod: i
^self allExisting inject: false into: [:o :v|o or: [v forcesInputMethod: i]]! !

!AllDomain methodsFor: 'accessing' stamp: 'GDK 7/7/2020 09:12:45'!
domains
	"Answer the value of domains"

	^ domains! !

!AllDomain methodsFor: 'accessing' stamp: 'GDK 7/7/2020 09:12:45'!
domains: anObject
	"Set the value of domains"

	domains _ anObject! !

!AllDomain methodsFor: 'accessing' stamp: 'GDK 7/7/2020 10:37:50'!
exists
domains size < 2 ifTrue: [^false].
^domains inject: true into: [:b :d|b and: [d exists]]! !

!LinkDomain methodsFor: 'accessing' stamp: 'GDK 7/6/2020 18:12:54'!
exists
^exists ! !

!LinkDomain methodsFor: 'accessing' stamp: 'GDK 7/6/2020 18:13:16'!
exists: anObject
	"Set the value of exists"
(thisContext sender receiver class == DomainLink) ifFalse: [^self].
	exists _ anObject! !

!WorldDomain methodsFor: 'as yet unclassified' stamp: 'GDK 7/6/2020 14:51:13'!
exists
^world == self runningWorld! !

!WorldDomain methodsFor: 'accessing' stamp: 'GDK 7/6/2020 14:51:22'!
world
	"Answer the value of world"

	^ world! !

!WorldDomain methodsFor: 'accessing' stamp: 'GDK 7/6/2020 14:51:22'!
world: anObject
	"Set the value of world"

	world _ anObject! !
