'From Cuis 5.0 [latest update: #4253] on 3 August 2020 at 8:58:23 pm'!
'Description '!
!provides: 'Projects' 1 1!
!requires: 'Domains' 1 0 nil!
SystemOrganization addCategory: #Projects!


!classDefinition: #ProjectDomain category: #Projects!
Domain subclass: #ProjectDomain
	instanceVariableNames: 'project'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Projects'!
!classDefinition: 'ProjectDomain class' category: #Projects!
ProjectDomain class
	instanceVariableNames: ''!

!classDefinition: #Project category: #Projects!
Object subclass: #Project
	instanceVariableNames: 'world initials name protected projectName'
	classVariableNames: 'Current'
	poolDictionaries: ''
	category: 'Projects'!
!classDefinition: 'Project class' category: #Projects!
Project class
	instanceVariableNames: ''!


!ProjectDomain methodsFor: 'accessing' stamp: 'GDK 7/6/2020 14:52:24'!
exists
^project == Project current! !

!ProjectDomain methodsFor: 'accessing' stamp: 'GDK 7/6/2020 14:52:09'!
project
	"Answer the value of project"

	^ project! !

!ProjectDomain methodsFor: 'accessing' stamp: 'GDK 7/6/2020 14:52:09'!
project: anObject
	"Set the value of project"

	project _ anObject! !

!Project methodsFor: 'accessing' stamp: 'GDK 7/6/2020 11:11:59'!
initials
	"Answer the value of initials"

	^ initials! !

!Project methodsFor: 'accessing' stamp: 'GDK 7/6/2020 11:11:59'!
initials: anObject
	"Set the value of initials"

	initials _ anObject! !

!Project methodsFor: 'accessing' stamp: 'GDK 7/31/2020 16:22:58'!
projectName
	"Answer the value of projectName"

	^ projectName! !

!Project methodsFor: 'accessing' stamp: 'GDK 7/31/2020 16:22:58'!
projectName: anObject
	"Set the value of projectName"

	projectName _ anObject! !

!Project methodsFor: 'accessing' stamp: 'GDK 7/6/2020 14:20:06'!
protected
	"Answer the value of protected"

	^ protected! !

!Project methodsFor: 'accessing' stamp: 'GDK 7/6/2020 14:20:07'!
protected: anObject
	"Set the value of protected"

	protected _ anObject! !

!Project methodsFor: 'accessing' stamp: 'GDK 7/31/2020 16:22:30'!
userName
^name! !

!Project methodsFor: 'accessing' stamp: 'GDK 7/31/2020 16:22:37'!
userName: anObject
	"Set the value of name"

	name _ anObject! !

!Project methodsFor: 'accessing' stamp: 'GDK 7/6/2020 09:59:10'!
world
	"Answer the value of world"

	^ world! !

!Project methodsFor: 'accessing' stamp: 'GDK 7/6/2020 09:59:10'!
world: anObject
	"Set the value of world"

	world _ anObject! !

!Project methodsFor: 'initialization' stamp: 'GDK 7/6/2020 17:26:07'!
enter
Domain allExisting do: [:d|(d blocksProject: self) ifTrue: [^self]].
Current ifNotNil: #enter.
{(Current ifNil: [Current _ Project new. Current world: self runningWorld. Current]). self runningWorld. Utilities authorName. Utilities authorInitials} elementsExchangeIdentityWith: {self. world. name. initials}. ! !

!Project methodsFor: 'initialization' stamp: 'GDK 7/31/2020 16:23:13'!
initialize
super initialize.
world _ PasteUpMorph new.
world worldState: ((Current ifNotNil: [:c|c world] ifNil: ["|w |UISupervisor whenUIinSafeState: [w _ self runningWorld]. [w] whileNil: []. w" self runningWorld]) instVarNamed: #worldState) shallowCopy.
initials _ Utilities authorInitials.
name _ Utilities authorName.
protected _ false.
projectName _ 'Untitled'.! !

!Project methodsFor: 'initialization' stamp: 'GDK 7/31/2020 16:57:10'!
showInPortal
SystemWindow new addMorph: (PortalMorph new target: world;yourself);openInWorld.! !

!Project class methodsFor: 'as yet unclassified' stamp: 'GDK 7/31/2020 11:43:24'!
current 
^Current  ifNil: [Current _ Project new. Current world: self runningWorld. Current]! !

!Project class methodsFor: 'as yet unclassified' stamp: 'GDK 7/31/2020 11:45:14'!
currentMaybe
^Current! !

!Domain methodsFor: '*Projects' stamp: 'GDK 7/6/2020 15:02:38'!
blocksProject: p
^false! !
