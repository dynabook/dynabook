'From Cuis 5.0 [latest update: #4253] on 6 August 2020 at 11:25:54 am'!
'Description '!
!provides: 'Buttons' 1 1!
SystemOrganization addCategory: #Buttons!


!classDefinition: #Button category: #Buttons!
Object subclass: #Button
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Buttons'!
!classDefinition: 'Button class' category: #Buttons!
Button class
	instanceVariableNames: ''!

!classDefinition: #CounterButton category: #Buttons!
Button subclass: #CounterButton
	instanceVariableNames: 'wrapped value'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Buttons'!
!classDefinition: 'CounterButton class' category: #Buttons!
CounterButton class
	instanceVariableNames: ''!

!classDefinition: #EventWrapperButton category: #Buttons!
Button subclass: #EventWrapperButton
	instanceVariableNames: 'wrapped value'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Buttons'!
!classDefinition: 'EventWrapperButton class' category: #Buttons!
EventWrapperButton class
	instanceVariableNames: ''!

!classDefinition: #MapButton category: #Buttons!
Button subclass: #MapButton
	instanceVariableNames: 'wrapped mapper'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Buttons'!
!classDefinition: 'MapButton class' category: #Buttons!
MapButton class
	instanceVariableNames: ''!

!classDefinition: #MultiButton category: #Buttons!
Button subclass: #MultiButton
	instanceVariableNames: 'all'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Buttons'!
!classDefinition: 'MultiButton class' category: #Buttons!
MultiButton class
	instanceVariableNames: ''!

!classDefinition: #SensorKeyButton category: #Buttons!
Button subclass: #SensorKeyButton
	instanceVariableNames: 'sensor key'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Buttons'!
!classDefinition: 'SensorKeyButton class' category: #Buttons!
SensorKeyButton class
	instanceVariableNames: ''!

!classDefinition: #SensorMouseButton category: #Buttons!
Button subclass: #SensorMouseButton
	instanceVariableNames: 'sensor sel'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Buttons'!
!classDefinition: 'SensorMouseButton class' category: #Buttons!
SensorMouseButton class
	instanceVariableNames: ''!

!classDefinition: #SensorMouseKeyButton category: #Buttons!
Button subclass: #SensorMouseKeyButton
	instanceVariableNames: 'sensor'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Buttons'!
!classDefinition: 'SensorMouseKeyButton class' category: #Buttons!
SensorMouseKeyButton class
	instanceVariableNames: ''!

!classDefinition: #ButtonSensor category: #Buttons!
EventSensor subclass: #ButtonSensor
	instanceVariableNames: 'buttonDict'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Buttons'!
!classDefinition: 'ButtonSensor class' category: #Buttons!
ButtonSensor class
	instanceVariableNames: ''!


!Button methodsFor: 'as yet unclassified' stamp: 'GDK 8/5/2020 13:22:30'!
emit: val
self triggerEvent: #emit with: val! !

!Button methodsFor: 'as yet unclassified' stamp: 'GDK 8/5/2020 13:05:28'!
getValue
^nil! !

!CounterButton methodsFor: 'accessing' stamp: 'GDK 8/6/2020 09:46:21'!
emit: v
super emit: (value _ v + value)! !

!CounterButton methodsFor: 'accessing' stamp: 'GDK 8/6/2020 09:46:07'!
value
^value! !

!CounterButton methodsFor: 'accessing' stamp: 'GDK 8/6/2020 09:46:07'!
value: anObject
	"Set the value of value"

	value _ anObject! !

!CounterButton methodsFor: 'accessing' stamp: 'GDK 8/6/2020 09:46:07'!
wrapped
	"Answer the value of wrapped"

	^ wrapped! !

!CounterButton methodsFor: 'accessing' stamp: 'GDK 8/6/2020 09:46:08'!
wrapped: anObject
	"Set the value of wrapped"
wrapped removeActionsWithReceiver: self.
	wrapped _ anObject.
	wrapped when: #emit send: #emit: to: self.! !

!CounterButton class methodsFor: 'as yet unclassified' stamp: 'GDK 8/6/2020 09:59:14'!
createFrom: b1 and: b2
| m1 m2 multi |
m1 _ MapButton new wrapped: b1;mapper: [:x|-1];yourself.
m2 _  MapButton new wrapped: b2;mapper: [:x|1];yourself.
multi _ MultiButton new all: {m1. m2};yourself.
^self new wrapped: multi;yourself! !

!EventWrapperButton methodsFor: 'accessing' stamp: 'GDK 8/6/2020 09:09:20'!
emit: v
super emit: (value _ v)! !

!EventWrapperButton methodsFor: 'accessing' stamp: 'GDK 8/6/2020 09:08:24'!
getValue
^value! !

!EventWrapperButton methodsFor: 'accessing' stamp: 'GDK 8/6/2020 09:08:16'!
value
^value! !

!EventWrapperButton methodsFor: 'accessing' stamp: 'GDK 8/6/2020 09:08:04'!
value: anObject
	"Set the value of value"

	value _ anObject! !

!EventWrapperButton methodsFor: 'accessing' stamp: 'GDK 8/6/2020 09:08:04'!
wrapped
	"Answer the value of wrapped"

	^ wrapped! !

!EventWrapperButton methodsFor: 'accessing' stamp: 'GDK 8/6/2020 09:08:55'!
wrapped: anObject
	"Set the value of wrapped"
wrapped removeActionsWithReceiver: self.
	wrapped _ anObject.
	wrapped when: #emit send: #emit: to: self.! !

!MapButton methodsFor: 'accessing' stamp: 'GDK 8/6/2020 09:47:27'!
emit: v
super emit: (mapper value: v)! !

!MapButton methodsFor: 'accessing' stamp: 'GDK 8/6/2020 09:47:17'!
mapper
	"Answer the value of mapper"

	^ mapper! !

!MapButton methodsFor: 'accessing' stamp: 'GDK 8/6/2020 09:47:17'!
mapper: anObject
	"Set the value of mapper"

	mapper _ anObject! !

!MapButton methodsFor: 'accessing' stamp: 'GDK 8/6/2020 09:46:51'!
wrapped
	"Answer the value of wrapped"

	^ wrapped! !

!MapButton methodsFor: 'accessing' stamp: 'GDK 8/6/2020 09:46:51'!
wrapped: anObject
	"Set the value of wrapped"
wrapped removeActionsWithReceiver: self.
	wrapped _ anObject.
	wrapped when: #emit send: #emit: to: self.! !

!MultiButton methodsFor: 'accessing' stamp: 'GDK 8/6/2020 09:55:54'!
all
	"Answer the value of all"

	^ all! !

!MultiButton methodsFor: 'accessing' stamp: 'GDK 8/6/2020 09:59:03'!
all: anObject
	"Set the value of all"
all ifNotNil: [all do: [:a|a removeActionsWithReceiver: self]].
	all _ anObject.
	all do: [:a|a when: #emit send: #emit: to: self]! !

!SensorKeyButton methodsFor: 'as yet unclassified' stamp: 'GDK 8/5/2020 13:12:09'!
getValue
^sensor keyboardPeek = key! !

!SensorKeyButton methodsFor: 'accessing' stamp: 'GDK 8/5/2020 13:24:39'!
emitE: evt
evt first = EventSensor eventTypeKeyboard ifTrue: [self emit: (evt at: 3)]! !

!SensorKeyButton methodsFor: 'accessing' stamp: 'GDK 8/5/2020 13:22:06'!
key
	"Answer the value of key"

	^ key! !

!SensorKeyButton methodsFor: 'accessing' stamp: 'GDK 8/5/2020 13:22:06'!
key: anObject
	"Set the value of key"

	key _ anObject! !

!SensorKeyButton methodsFor: 'accessing' stamp: 'GDK 8/5/2020 13:22:06'!
sensor
	"Answer the value of sensor"

	^ sensor! !

!SensorKeyButton methodsFor: 'accessing' stamp: 'GDK 8/5/2020 13:23:34'!
sensor: anObject
	"Set the value of sensor"
sensor removeActionsWithReceiver: self.
	sensor _ anObject.
	sensor when: #inputEvent send: #emitE: to: self.! !

!SensorMouseButton methodsFor: 'as yet unclassified' stamp: 'GDK 8/5/2020 13:25:28'!
emitE: evt
evt first = EventSensor eventTypeMouse ifTrue: [self emit: self getValue]! !

!SensorMouseButton methodsFor: 'as yet unclassified' stamp: 'GDK 8/5/2020 13:21:53'!
getValue
^sensor peekMousePt perform: sel! !

!SensorMouseButton methodsFor: 'accessing' stamp: 'GDK 8/5/2020 13:22:12'!
sel
	"Answer the value of sel"

	^ sel! !

!SensorMouseButton methodsFor: 'accessing' stamp: 'GDK 8/5/2020 13:22:12'!
sel: anObject
	"Set the value of sel"

	sel _ anObject! !

!SensorMouseButton methodsFor: 'accessing' stamp: 'GDK 8/5/2020 13:22:11'!
sensor
	"Answer the value of sensor"

	^ sensor! !

!SensorMouseButton methodsFor: 'accessing' stamp: 'GDK 8/5/2020 13:24:55'!
sensor: anObject
	"Set the value of sensor"
sensor removeActionsWithReceiver: self.
	sensor _ anObject.
	sensor when: #inputEvent send: #emitE: to: self.! !

!SensorMouseKeyButton methodsFor: 'as yet unclassified' stamp: 'GDK 8/5/2020 15:53:37'!
getValue
^sensor mouseButtons! !

!ButtonSensor methodsFor: 'accessing' stamp: 'GDK 8/5/2020 15:48:37'!
buttonDict
	"Answer the value of buttonDict"

	^ buttonDict! !

!ButtonSensor methodsFor: 'accessing' stamp: 'GDK 8/5/2020 15:48:37'!
buttonDict: anObject
	"Set the value of buttonDict"

	buttonDict _ anObject! !

!ButtonSensor methodsFor: 'accessing' stamp: 'GDK 8/5/2020 16:47:22'!
createButtonDict: oldSensor
buttonDict _( ((0 to: 256 ) collect: [:x|x -> SensorKeyButton new sensor: oldSensor;key: x asCharacter;yourself]), {#x -> SensorMouseButton new sensor: oldSensor;sel: #x;yourself. #y -> SensorMouseButton new sensor: oldSensor;sel: #y;yourself. #mouse -> SensorMouseKeyButton new sensor: oldSensor;yourself}) asDictionary! !

!ButtonSensor methodsFor: 'accessing' stamp: 'GDK 8/5/2020 15:56:21'!
primGetNextEvent: array
	"Store the next OS event available into the provided array.
	Essential. If the VM is not event driven the ST code will fall
	back to the old-style mechanism and use the state based
	primitives instead."
	| kbd buttons modifiers pos mapped |
	"Simulate the events"
	array at: 1 put: EventSensor eventTypeNone. "assume no more events"

	"First check for keyboard"
	kbd _ (((self buttonDict values select: [:b|b getValue and: [b isKindOf: SensorKeyButton]]) collect: [:b|b key]) select: [:k|k isKindOf: Character]) at: 1.
	kbd ifNotNil: [
		"simulate keyboard event"
		array at: 1 put: EventSensor eventTypeKeyboard. "evt type"
		array at: 2 put: Time localMillisecondClock. "time stamp"
		array at: 3 put: (kbd bitAnd: 255). "char code"
		array at: 4 put: EventSensor eventKeyChar. "key press/release"
		array at: 5 put: (kbd bitShift: -8). "modifier keys"
		^self].

	"Then check for mouse"
	buttons _  (((self buttonDict values select: [:b|b isKindOf: SensorMouseKeyButton]) collect: [:b|b getValue])) at: 1..
	pos _ (self buttonDict at: #x) getValue @ (self buttonDict at: #y) getValue.
	modifiers _ buttons bitShift: -3.
	buttons _ buttons bitAnd: 7.
	mapped _ self mapButtons: buttons modifiers: modifiers.
	(pos = mousePosition and:[(mapped bitOr: (modifiers bitShift: 3)) = mouseButtons])
		ifTrue:[^self].
	array 
		at: 1 put: EventSensor eventTypeMouse;
		at: 2 put: Time localMillisecondClock;
		at: 3 put: pos x;
		at: 4 put: pos y;
		at: 5 put: mapped;
		at: 6 put: modifiers.
! !

!ButtonSensor methodsFor: 'accessing' stamp: 'GDK 8/5/2020 16:51:42'!
unwrap
self becomeForward: self buttonDict values asArray first sensor.! !
