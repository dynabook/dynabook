mkdir -p /dynabook/x11/0
wget https://git.savannah.gnu.org/cgit/guix.git/plain/etc/guix-install.sh | sh -
mkdir -p /dynabook/fs
mount /dev/sda1 /dynabook/fs
cat <<EOF
dynabook/**
dev/**
proc/**
EOF | /.gitignore
bwrap --dev-bind /dynabook/fs / --dev-bind /var/run/docker.sock /var/run/docker.sock sh -c "dockerd" &
bwrap --dev-bind / / --bind  /dynabook/x11/0 /tmp/.X11-unix --bind /dynabook/fs/usr/bin/Xorg /usr/bin/X --bind /dynabook/fs/usr/bin/startx /usr/bin/startx sh -c "startx & && squeak --disk --dev /dev" &
bwrap --bind / / --tmpfs /proc --bind /proc/$(pidof squeak) /proc/squeak sshd -D &
